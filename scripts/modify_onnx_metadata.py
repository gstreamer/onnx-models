import sys
import json
import onnx
from collections import OrderedDict

def modify_metadata_onnx(model_path, metadata_path=None, remove=False):
    # Load the ONNX model
    model = onnx.load(model_path)

    # If metadata file is provided
    if metadata_path:
        with open(metadata_path, 'r') as f:
            metadata = json.load(f)

        for key, value in metadata.items():
            # Check if the key already exists in the metadata
            for prop in model.metadata_props:
                if prop.key == key:
                    # Key exists, remove it from the metadata
                    model.metadata_props.remove(prop)
                    break
            
            # Add the metadata property if not remove option and value is provided
            if not remove and value:
                model.metadata_props.append(onnx.StringStringEntryProto(key=key, value=value))

    # Save the modified ONNX model
    onnx.save(model, model_path)

    # Print the updated metadata
    updated_model = onnx.load(model_path)
    updated_metadata = {prop.key: prop.value for prop in updated_model.metadata_props}
    print(f"Updated metadata: {updated_metadata}")

def dump_output_layer_names(model_path, output_json_path):
    # Load the ONNX model
    model = onnx.load(model_path)
    
    # Get output layer names
    output_layer_names = [output.name for output in model.graph.output]
    output_layer_names_json = json.dumps(OrderedDict((name, name) for name in output_layer_names))

    # Write the output layer names to a json file
    with open(output_json_path, 'w') as f:
        f.write(output_layer_names_json)

if __name__ == '__main__':
    if len(sys.argv) < 3:
        print("Usage: python modify_onnx_metadata.py <model_path> <command> <optional_args>")
        sys.exit(1)

    model_path = sys.argv[1]
    command = sys.argv[2]

    if command == "add":
        metadata_path = sys.argv[3]
        modify_metadata_onnx(model_path, metadata_path=metadata_path, remove=False)
    elif command == "remove":
        metadata_path = sys.argv[3]
        modify_metadata_onnx(model_path, metadata_path=metadata_path, remove=True)
    elif command == "dump":
        output_json_path = sys.argv[3]
        dump_output_layer_names(model_path, output_json_path)

