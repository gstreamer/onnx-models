# GStreamer Analytics Experimentation Data
This repository contain data and scripts useful to experiment with GStreamer analytics.

# Cloning
This repository uses Git LFS to manage large ONNX model files. Please install [Git LFS](https://git-lfs.com/)
If a model file is not fully donwloaded after clone, it may be manually fetched using command
`git lfs fetch && git lfs checkout`

# Structure
What you can find here.

## Images
Samples you can use to experiment that are referred in documentation.

## Labels
Label files used by classification models

## Models
Models you can use to experiment with GStreamer analytics.

### Fruits Classification
This model allow to classify image of the following 10 classes of fruits:
- Apple
- Banana
- Avocado
- Cherry
- Kiwi
- Mango
- Orange
- Pineapple
- Strawberries
- Watermelon.

#### Inputs

|name |batch|width|height|channels|sample format|pixel format|datatype|components range|
|---  |---  |---  |---   |---     |---          |---         |---     |---             |
|x    |1    |200  |250   |3       |hwc          |RGB         |float32 |0.0 - 255.0     |

#### Outputs
|name   |dimensions|datatype|
|---    |---       |---     |
|dense_1|1x10      |float32 |

#### Links
- [Onnx model](https://gitlab.collabora.com/gstreamer/onnx-models/-/blob/9b895228ac560b44ad9a48d46de7cabb1eb1f072/models/fruit_classification.onnx)
- [Labels file](https://gitlab.collabora.com/gstreamer/onnx-models/-/blob/9b895228ac560b44ad9a48d46de7cabb1eb1f072/labels/fruit_classification_classes.txt)

### Fruits Segmentation
This model allow to detect and segment (instance-segmentation) Strawberries.

*This model does not do classification*.

#### Inputs

|name   |batch|width|height  |channels|sample format|pixel format|datatype|components range|
| ---   |---  |---  |---     |---     |---          |---         |---     |---             |
|images |1    |1024  |1024   |3       |chw          |RGB         |float32 |0.0 - 1.0       |


#### Outputs

|name       |dimensions     |datatype|
|---        |---            |---     |
|output0    |1x37x21504     |float32 |
|output1    |1x32x256x256   |float32 |

#### Links
- [Onnx model](https://gitlab.collabora.com/gstreamer/onnx-models/-/blob/42554e06b6129e75af905dd81558ebee9ba0fb49/models/segmentation.onnx)

### SSD Object Detection
This model allow to detect and classify objects.
Object it can detect and classify can be found here: [COCO](https://gitlab.collabora.com/gstreamer/onnx-models/-/blob/acc119dd795be5e8c756457dc04507a5d9b8e768/labels/COCO_classes.txt)

#### Inputs

|name           |batch|width |height |channels|sample format|pixel format|datatype|components range|
| ---           |---  |---   |---    |---     |---          |---         |---     |---             |
|image_tensor:0 |1    |640   |383    |3       |hwc          |RGB         |Int8    |0 - 255         |


#### Outputs

|name                   |dimensions             |datatype|
|---                    |---                    |---     |
|num_detections:0       |1x1                    |float32 |
|detection_boxes:0      |1 x num_detection x 4  |float32 |
|detection_classes:0    |1 x num_detection      |float32 |
|detection_scores:0     |1 x num_detection      |float32 |

#### Links
- [Onnx model](https://gitlab.collabora.com/gstreamer/onnx-models/-/blob/acc119dd795be5e8c756457dc04507a5d9b8e768/models/ssd_mobilenet_v1_coco.onnx)
- [Labels file](https://gitlab.collabora.com/gstreamer/onnx-models/-/blob/acc119dd795be5e8c756457dc04507a5d9b8e768/labels/COCO_classes.txt)

## Scripts
Using this [script](https://gitlab.collabora.com/gstreamer/onnx-models/-/blob/9b895228ac560b44ad9a48d46de7cabb1eb1f072/scripts/modify_onnx_metadata.py)
and a configuration file we can embed the tensor-ids/names inside to Onnx model
to allow GStreamer Tensor Decoder to identify the tensors it can decode.

### Prerequisites
- Python3
- Onnx python package
- [optional] Netron 

``` Bash
python -m venv ~/.venv/myenv
source ~/.venv/myenv/bin/activate
pip install onnx netron
```

### Embedding Tensor-Ids/Names Into Onnx Model
Running the following command from a virtualenv with prerequisites will embed
the tensor-names into the Onnx format file as metadata that onnxinference
element will will retrieve and append to tensors allowing tensor decoders to 
identify tensors it can handle. "path_to_model.onnx" should be the path to the
model where you want to embed tensor-ids/names. "path_to_json_config.json" is
the path to a file that define the mapping between existing outputs-name and 
tensor-ids/names.


#### Extracting Onnx outputs-name
The same script modify_onnx_metadata.py allow to list output-names.

``` Bash
python modify_onnx_metadata.py path_to_model.onnx dump out.json
```

After using the "dump" command, the file will contain the names of the output-names.

Example using fruit classification model:
``` Bash
python modify_onnx_metadata.py fruit_classification.onnx dump out.json
cat out.json
{"dense_1": "dense_1"}
```
The key, left of `:` is the name of the output.

If the model has multiple outputs you will have multiple key-value pairs

Example using ssd_mobilenet_v1_coco.onnx:

``` Json
{
    "detection_boxes:0": "detection_boxes:0",
    "detection_classes:0": "detection_classes:0",
    "detection_scores:0": "detection_scores:0",
    "num_detections:0": "num_detections:0"
}
```

Note the output-names are duplicated in the value field and this is to
create a template for defining the mapping between output-names and
tensor-ids/names.

#### Creating the Json configuration File
This file map output-names to tensor-ids/names in a Json file using JSON-Object
structure.

``` Json
{
    "output-name-1":"corresponding tensor-id/name",
    ...
}
```

Example using ssd_mobilenet_v1_coco.onnx:
``` Json
{
    "detection_boxes:0": "Gst.Model.ObjectDetector.Boxes",
    "detection_classes:0": "Gst.Model.ObjectDetector.Classes",
    "detection_scores:0": "Gst.Model.ObjectDetector.Scores",
    "num_detections:0": "Gst.Model.ObjectDetector.NumDetections"
}
```

Example using fruit_classification.onnx:
``` Json
{
    "dense_1":"Gst.Model.Collabora.classifier.fruits"
}
```

Any string can be used for the value field, but the tensor-decoder need
to know this tensor-ids/name to be able to identify the tensor it can handle.
By doing a gst-inspect on a tensor-decoder and reviewing the property you 
should find properties with a pattern "tensor-name-{XYZ}" where the default
value can be used for tensor-name in the Json configuration file. If there's no
properties, you'll have to look at the tensor-decoder source code for a string
starting with "Gst.Model." or by looking at section 
[GStreamer Tensor-Decoders](#gstreamer-tensor-decoders). Note that when the 
property is present, it can also be updated with the string of your choice to 
match a mapping.

#### Embedding Tensor-ids/names in the Onnx model file
Once you have the Json configuration file, running the following command will
embed the output-names,tensor-ids/names mapping into the Onnx file as model 
metadata. GStreamer onnxinference element will use this mapping to generate a 
id (GQuark) from the tensor-name and attach this id to every tensor produce by
the corresponding output layer, identified by output-name. You can also update
the mapping by modifying the value of a key:value pair in the Json
configuration file. 

``` Bash
python modify_onnx_metadata.py path_to_model.onnx add path_to_json_config.json
```

#### Netron
Netron is a tool that allow you to read Onnx file and others. By clicking on the
input layer, you be able to see information about the models input layers,
output layers and model metadata including the mapping added using the script 
modify_onnx_metadata.py

#### GStreamer Tensor Decoders
All the models in this repository have a tensor-decoder element able to decoder
tensors produced from the inference process. The table bellow list  
the models of this repository, tensor-decoder able to handle tensors from the 
model, the output-names and the default tensor-ids/names mapping

|Model                                      | Output-Names           | GStreamer Tensor-Decoder    | Tensor-Decoder Default Tensor-ids/names   |
|---                                        |---                     |---                          |---                                        |
|fruit_classification.onnx                  | dense_1                | classifier_tensor_decoder   | "Gst.Model.Collabora.classifier.fruits"   |
|yolov8s-seg.onnx OR segmentation.onnx      | output0                | yolo_tensor_decoder         | "Gst.Model.Yolo.Segmentation.Masks"    |
|                                           | output1                | yolo_tensor_decoder         | "Gst.Model.Yolo.Segmentation.Logits"   |
|ssd_mobilenet_v1_coco.onnx                 | detection_boxes:0      | ssdobjectdetector           | "Gst.Model.ObjectDetector.Boxes"          |
|                                           | detection_classes:0    | ssdobjectdetector           | "Gst.Model.ObjectDetector.Classes"        |
|                                           | detection_scores:0     | ssdobjectdetector           | "Gst.Model.ObjectDetector.Scores"         |
|                                           | num_detections:0       | ssdobjectdetector           | "Gst.Model.ObjectDetector.NumDetections"  |
